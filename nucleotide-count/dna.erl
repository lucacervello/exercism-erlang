-module(dna).
-export([count/2, nucleotide_counts/1]).

-define(NUCLEOTIDES, ["A", "T", "C", "G"]).


count(Str, [Char|_]) ->
    case lists:member(Char, [X || [X|_] <- ?NUCLEOTIDES]) of
        false -> error("Invalid nucleotide");
        true -> length(lists:filter(fun (X) -> X == Char end, Str))
    end.

nucleotide_counts(Str) ->
    lists:map(fun (X) -> {X ,count(Str, X)} end, ?NUCLEOTIDES).
