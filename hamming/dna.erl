-module(dna).
-export([hamming_distance/2]).

hamming_distance_acc(Dna, Dna, Acc) ->
    Acc;

hamming_distance_acc([], [], Acc) ->
    Acc;

hamming_distance_acc([X | Xs], [Y | Ys], Acc) ->
    case X == Y of
        true ->
            hamming_distance_acc(Xs, Ys, Acc);
        false ->
            hamming_distance_acc(Xs, Ys, (1 + Acc))
    end.

hamming_distance(Dna1, Dna2) -> 
    hamming_distance_acc(Dna1, Dna2, 0). 
