-module(anagram).
-export([find/2]).

is_anagram(Name, OtherName) ->
    N = string:to_lower(Name),
    On = string:to_lower(OtherName),
    if
        N == On -> false;
        true -> lists:sort(N) == lists:sort(On)
    end.

find(Name, Names) ->
    lists:filter(fun (X) -> is_anagram(Name, X) end, Names).
