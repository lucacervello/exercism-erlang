-module(grains).
-export([square/1, total/0]).

board() ->
    lists:seq(1, 64).

square(N) ->
    trunc(math:pow(2, N - 1)).

total() ->
    lists:foldl(fun(X, Acc) -> square(X) + Acc end, 0, board()).
