-module(space_age).
-export([ageOn/2]).

-define(EARTH_YEAR_IN_SECONDS, 31557600).

ageOn(earth, Seconds) ->
    Seconds / ?EARTH_YEAR_IN_SECONDS;
ageOn(mercury, Seconds) ->
    Seconds / (?EARTH_YEAR_IN_SECONDS * 0.2408467);
ageOn(venus, Seconds) ->
    Seconds / (?EARTH_YEAR_IN_SECONDS * 0.61519726) ;
ageOn(mars, Seconds) ->
    Seconds / (?EARTH_YEAR_IN_SECONDS * 1.8808158);
ageOn(jupiter, Seconds) ->
    Seconds / (?EARTH_YEAR_IN_SECONDS * 11.862615);
ageOn(saturn, Seconds) ->
    Seconds / (?EARTH_YEAR_IN_SECONDS * 29.447498);
ageOn(uranus, Seconds) ->
    Seconds / (?EARTH_YEAR_IN_SECONDS * 84.016846);
ageOn(neptune, Seconds) ->
    Seconds / (?EARTH_YEAR_IN_SECONDS * 164.79132).





