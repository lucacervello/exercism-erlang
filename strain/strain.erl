-module(strain).
-export([keep/2, discard/2]).

keep(_Pred, []) ->
    [];
keep(Pred, [H | T]) ->
    case Pred(H) of
        true ->
            [H | keep(Pred, T)];
        false ->
            keep(Pred, T)
    end.

discard(Pred, List) ->
    keep(fun (X) -> not Pred(X) end, List).
