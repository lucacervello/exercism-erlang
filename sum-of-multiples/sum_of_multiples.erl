-module(sum_of_multiples).
-export([sumOfMultiples/2]).

filterMultiplesFromList(X, Ls) ->
    lists:any(fun (Y) -> X rem Y == 0 end, Ls).

sumOfMultiples(Multipliers, Range) ->
    List = lists:seq(1, Range - 1),
    ListFiltered = lists:filter(fun (X) -> filterMultiplesFromList(X, Multipliers) end, List),
    lists:sum(ListFiltered).

