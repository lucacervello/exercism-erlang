-module(atbash_cipher).
-export([encode/1, decode/1]).

add_blanks([], _) ->
    [];

add_blanks([H|T], N) when N == 5  ->
    [H] ++ [$\s] ++ (add_blanks(T, 1));

add_blanks([H|T], N) ->
    [H] ++ (add_blanks(T, N+1)).

encrypt(X, Alphabet) ->
    ReverseAlphabet = lists:reverse(Alphabet),
    case lists:member(X, lists:seq($1, $9)) of
        true -> X;
        false -> lists:nth(string:chr(Alphabet, X), ReverseAlphabet)
    end.

encode(CipherText) ->
    string:strip(add_blanks(decode(CipherText), 1), right, $\s).

decode(PlainText) ->
    Alphabet = lists:seq($a, $z),
    PT = lists:filter(fun (X) -> lists:member(X, Alphabet ++ lists:seq($1, $9)) end, string:to_lower(PlainText)),
    lists:map(fun (X) -> encrypt(X, Alphabet) end, PT).

