
-module(hello_world).
-export([greet/1, greet/0]).

greet(Name) ->
    "Hello, " ++ Name ++ "!".

greet() ->
    greet("World").

